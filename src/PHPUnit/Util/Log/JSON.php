<?php

declare(strict_types=1);

namespace Codeception\PHPUnit\Util\Log;

use PHPUnit\Framework\AssertionFailedError;
use PHPUnit\Framework\SelfDescribing;
use PHPUnit\Framework\Test as PHPUnitTest;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\TestFailure;
use PHPUnit\Framework\TestListener;
use PHPUnit\Framework\TestSuite;
use PHPUnit\Framework\Warning;
use PHPUnit\Util\Filter;
use PHPUnit\Util\Printer;
use Throwable;
use function array_walk_recursive;
use function count;
use function is_string;
use function json_encode;
use function mb_convert_encoding;

/**
 * A TestListener that generates JSON messages.
 */
class JSON extends Printer implements TestListener
{
    /**
     * @var string
     */
    protected $currentTestSuiteName = '';
    /**
     * @var string|array Array containing class name and method name
     */
    protected $currentTestName = '';
    /**
     * @var bool
     */
    protected $currentTestPass = true;
    /**
     * @var array
     */
    protected $logEvents = [];

    /**
     * An error occurred.
     */
    public function addError(PHPUnitTest $test, Throwable $t, float $time): void
    {
        $this->writeCase(
            'error',
            $time,
            Filter::getFilteredStacktrace($t, false),
            TestFailure::exceptionToString($t),
            $test
        );

        $this->currentTestPass = false;
    }

    /**
     * A warning occurred.
     */
    public function addWarning(PHPUnitTest $test, Warning $e, float $time): void
    {
        $this->writeCase(
            'warning',
            $time,
            Filter::getFilteredStacktrace($e, false),
            TestFailure::exceptionToString($e),
            $test
        );

        $this->currentTestPass = false;
    }

    /**
     * A failure occurred.
     */
    public function addFailure(PHPUnitTest $test, AssertionFailedError $e, float $time): void
    {
        $this->writeCase(
            'fail',
            $time,
            Filter::getFilteredStacktrace($e, false),
            TestFailure::exceptionToString($e),
            $test
        );

        $this->currentTestPass = false;
    }

    /**
     * Incomplete test.
     */
    public function addIncompleteTest(PHPUnitTest $test, Throwable $t, float $time): void
    {
        $this->writeCase(
            'error',
            $time,
            Filter::getFilteredStacktrace($t, false),
            'Incomplete Test: ' . $t->getMessage(),
            $test
        );

        $this->currentTestPass = false;
    }

    /**
     * Risky test.
     */
    public function addRiskyTest(PHPUnitTest $test, Throwable $t, float $time): void
    {
        $this->writeCase(
            'error',
            $time,
            Filter::getFilteredStacktrace($t, false),
            'Risky Test: ' . $t->getMessage(),
            $test
        );

        $this->currentTestPass = false;
    }

    /**
     * Skipped test.
     */
    public function addSkippedTest(PHPUnitTest $test, Throwable $t, float $time): void
    {
        $this->writeCase(
            'error',
            $time,
            Filter::getFilteredStacktrace($t, false),
            'Skipped Test: ' . $t->getMessage(),
            $test
        );

        $this->currentTestPass = false;
    }

    /**
     * A testsuite started.
     */
    public function startTestSuite(TestSuite $suite): void
    {
        $this->currentTestSuiteName = $suite->getName();
        $this->currentTestName      = '';

        $this->addLogEvent(
            [
                'event' => 'suiteStart',
                'suite' => $this->currentTestSuiteName,
                'tests' => count($suite)
            ]
        );
    }

    /**
     * A testsuite ended.
     */
    public function endTestSuite(TestSuite $suite): void
    {
        $this->currentTestSuiteName = '';
        $this->currentTestName      = '';
        $this->writeArray($this->logEvents);
    }

    /**
     * A test started.
     */
    public function startTest(PHPUnitTest $test): void
    {
        $this->currentTestName = $this->describe($test);
        $this->currentTestPass = true;

        $this->addLogEvent(
            [
                'event' => 'testStart',
                'suite' => $this->currentTestSuiteName,
                'test'  => $this->currentTestName
            ]
        );
    }

    /**
     * A test ended.
     */
    public function endTest(PHPUnitTest $test, float $time): void
    {
        if ($this->currentTestPass) {
            $this->writeCase('pass', $time, [], '', $test);
        }
    }

    protected function writeCase(string $status, float $time, array $trace = [], string $message = '', ?PHPUnitTest $test = null): void
    {
        $output = '';
        // take care of TestSuite producing error (e.g. by running into exception)
        if ($test instanceof TestCase && $test->hasOutput()) {
            $output = $test->getActualOutput();
        }
        $this->addLogEvent(
            [
                'event'   => 'test',
                'suite'   => $this->currentTestSuiteName,
                'test'    => $this->currentTestName,
                'status'  => $status,
                'time'    => $time,
                'trace'   => $trace,
                'message' => $this->convertToUtf8($message),
                'output'  => $output,
            ]
        );
    }

    protected function addLogEvent(array $eventData = []): void
    {
        if (count($eventData) > 0) {
            $this->logEvents[] = $eventData;
        }
    }

    public function writeArray(array $buffer): void
    {
        array_walk_recursive(
            $buffer, function (&$input) {
            if (is_string($input)) {
                $input = $this->convertToUtf8($input);
            }
        }
        );

        $this->write(json_encode($buffer, JSON_PRETTY_PRINT));
    }

    private function convertToUtf8(string $string): string
    {
        return mb_convert_encoding($string, 'UTF-8');
    }

    private function describe(PHPUnitTest $test): array
    {
        if ($test instanceof TestCase) {
            return [get_class($test), $test->getName()];
        }

        if ($test instanceof SelfDescribing) {
            return ['', $test->toString()];
        }

        return ['', get_class($test)];
    }
}
